from pathlib import Path

import pandas as pd


DATA_DIR = Path().joinpath("data")

JALALI_MONTHS = {
    'فروردين': 1,
    'ارديبهشت': 2,
    'خرداد': 3,
    'تير': 4,
    'مرداد': 5,
    'شهريور': 6,
    'مهر': 7,
    'آبان': 8,
    'آذر': 9,
    'دی': 10,
    'بهمن': 11,
    'اسفند': 12,
}


def create_annual_table():
    table = pd.read_csv(DATA_DIR.joinpath("raw_data", "annual.csv"))
    table.columns = ["Year", "CPI", "Annual_Inflation"]
    table["Year"] = table["Year"].astype(int)
    table = table.set_index("Year")
    table = table.sort_index()
    return table


def create_monthly_table():
    monthly_tables = DATA_DIR.joinpath("raw_data", "monthly_tables")
    df_list = []
    years = []
    for path in monthly_tables.iterdir():
        dataframe = pd.read_csv(path)
        dataframe = clean_monthly_dataframe(dataframe)
        df_list.append(dataframe)
        years.append(int(path.name.replace(".csv", "")))
    merged_table = pd.concat(df_list, keys=years, names=["Year", "Month"])
    merged_table = merged_table.sort_index()

    ptop = ((merged_table["CPI"] / merged_table["CPI"].shift(12) - 1) * 100).round(3)
    merged_table["Point_to_Point_Inflation"] = ptop

    year_month = merged_table.index.to_frame().astype(str)
    month_series = year_month["Month"].apply(lambda month: f"0{month}" if len(month)==1 else month)
    merged_table.insert(0, "Year-Month", year_month["Year"] + "-" + month_series)

    return merged_table


def clean_monthly_dataframe(dataframe):
    dataframe.columns = ["Month", "CPI", "Annual_Inflation"]
    dataframe["Month"] = dataframe["Month"].map(JALALI_MONTHS)
    dataframe = dataframe.set_index("Month")
    return dataframe


if __name__ == "__main__":
    merged_data_dir = DATA_DIR.joinpath("merged_data")
    merged_data_dir.mkdir(exist_ok=True)
    annual_table = create_annual_table()
    annual_table.to_csv(merged_data_dir.joinpath("annual_table.csv"))
    monthly_table = create_monthly_table()
    monthly_table.to_csv(merged_data_dir.joinpath("monthly_table.csv"))
